import com.sun.source.util.JavacTask;
import javax.tools.*;
import java.io.*;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class JavaREPL {
    private static final String temporaryDirectory = System.getProperty("java.io.tmpdir");
    private static ClassLoader classLoader;
    private static ArrayList<String> replHistory = new ArrayList<>();
    private static StringBuilder replHelp = new StringBuilder("Available Commands:\n");
    private static StringBuilder replIntro = new StringBuilder("Welcome to Simple Java REPL.\n");

    /*
        This is the main method
     */
    public static void main(String[] args) throws IOException {
        exec(new InputStreamReader(System.in));
    }

    /*
        This method reads each and every line inputted by the user and performs the execution of statement accordingly
        Each statement (if valid)  is converted to java source code and stored in the file else storing the blank java source file
     */
    private static void exec(Reader r) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(r);
        NestedReader nestedReader = new NestedReader(bufferedReader);

        replHelp.append("\t.help - show commands options.\n");
        replHelp.append("\t.quit - quit REPL session.\n");
        replHelp.append("\t.history - show all commands you inputted in current session.\n");

        replIntro.append(".help (for help)\t.quit (for quit)");
        System.out.println(replIntro);

        int classNumber = 0;
        while (true) {
            System.out.print("java-repl> ");
            String nestedString = nestedReader.getNestedString();

            if(nestedString != null && !nestedString.equals("")){
                replHistory.add(nestedString);
            }

            if(nestedString != null && nestedString.equals(".quit")) {
                System.exit(0);
            }
            else if(nestedString != null && nestedString.equals(".help")){


                System.out.println(replHelp);
                nestedString = "";
            }
            else if(nestedString != null && nestedString.equals(".history")){
                System.out.println("Command History:");
                for(int counter = 0; counter < replHistory.size(); counter++){
                    System.out.printf("\t%d. %s\n", counter+1, replHistory.get(counter));
                }
                nestedString = "";
            }

            if (nestedString == null)
                return;
            nestedString = nestedString.trim();

            if(nestedString.startsWith("print") && nestedString.endsWith(";")){
                int index = nestedString.lastIndexOf(";");
                String printStat = nestedString.substring(0, index);
                int length = "print ".length();
                nestedString = "System.out.println(" + printStat.substring(length) + ");";
            }
            else if(nestedString.startsWith("print")){
                int length = "print ".length();
                nestedString = "System.out.println(" + nestedString.substring(length) + ");";
            }

            if(nestedString.equals(""))
                continue;

            String tempSourceFileName = "JavaRepl.java";
            String definition = "public static " + nestedString;
            String statement = "";
            String code = getCode("Test", "", "", definition, "");
            writeToFile(tempSourceFileName, code);
            String errorParse = compileCode(tempSourceFileName, "parse");
            if(errorParse != null){
                statement = nestedString;
                definition = "";
            }

            String nameClass = "Interpreter_" + classNumber;
            int superClassNumber = classNumber - 1;
            String superClass = "";
            if(classNumber > 0) {
                superClass = "Interpreter_" + superClassNumber;
                code = getCode(nameClass, " extends ", superClass, definition, statement);
            }
            else
                code = getCode(nameClass, "", "", definition, statement);

            String fileName = nameClass + ".java";
            writeToFile(fileName, code);

            String errorCompile = compileCode(fileName, "compile");
            if(errorCompile != null){
                System.err.print(errorCompile);
                if(classNumber > 0)
                    code = getCode(nameClass, " extends ", superClass, "", "");
                else
                    code = getCode(nameClass, "", "", "", "");
                writeToFile(fileName, code);
            }
            else{
                exec(nameClass, classNumber);
            }
            classNumber++;
        }
    }

    /*
        This method provides the class definition for each statement
     */
    private static String getCode(String nameClass, String extend, String superClass, String definition, String statement)
    {
        StringBuilder javaSourceCode = new StringBuilder("import java.io.*;\n");
        javaSourceCode.append("import java.util.*;\n");
        javaSourceCode.append("public class ");
        javaSourceCode.append(nameClass);
        javaSourceCode.append(extend);
        javaSourceCode.append(superClass);
        javaSourceCode.append("\n{\n");
        javaSourceCode.append(definition);
        javaSourceCode.append("\n");
        javaSourceCode.append("public static void exec() {\n");
        javaSourceCode.append(statement);
        javaSourceCode.append("\n}\n}\n");

        return String.valueOf(javaSourceCode);
    }

    /*
        This method compiles or parses the java code and returns the error message
     */
    private static String compileCode(String fileName, String option) throws IOException{
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        DiagnosticCollector diagnosticsCollector = new DiagnosticCollector();
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnosticsCollector, null, null);

        String dirFile = temporaryDirectory + File.separator + fileName;
        Iterable fileObjects = fileManager.getJavaFileObjectsFromStrings(Arrays.asList(dirFile));
        String classPath = "." + System.getProperty("path.separator") + temporaryDirectory;
        Iterable<String> options = Arrays.asList("-d", temporaryDirectory, "-cp", classPath);

        JavacTask task = (JavacTask) compiler.getTask(null, fileManager, diagnosticsCollector, options, null, fileObjects);

        if(option.equals("parse"))
            task.parse();
        else
            task.call();

        StringBuilder errorStringBuilder = new StringBuilder(100);
        if(diagnosticsCollector.getDiagnostics().size() == 0)
            return null;

        List<Diagnostic> diagnosticList = diagnosticsCollector.getDiagnostics();
        for(Diagnostic diagnostic: diagnosticList){
            errorStringBuilder.append("line ").append(diagnostic.getLineNumber()).append(": ").append(diagnostic.getMessage(Locale.ENGLISH)).append("\n");
        }

        return errorStringBuilder.toString();
    }

    /*
    This method writes the class definition in a java file stored in the temporary directory
    */
    private static void writeToFile(String fileName, String code){
        String dirFile = temporaryDirectory + File.separator + fileName;
        try(BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dirFile))))
        {
            bufferedWriter.write(code);
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /*
        This method invokes the exec() method of each class file created
     */
    private static void exec(String nameClass, int classNum) {
        try{
            if(classNum == 0){
                ClassLoader parentClassLoading = ClassLoader.getSystemClassLoader();
                URL url = new File(temporaryDirectory).toURI().toURL();
                classLoader = new URLClassLoader(new URL[] {url}, parentClassLoading);  //should be one only; instead of creating class loader every time
            }
            Class actualClass = classLoader.loadClass(nameClass);
            Method declaredMethod = actualClass.getDeclaredMethod("exec", (Class[])null);
            declaredMethod.invoke(null, (Object[]) null);
        }catch(Exception e){
            e.printStackTrace(System.err);
        }
    }
}