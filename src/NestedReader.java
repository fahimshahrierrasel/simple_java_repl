import java.io.BufferedReader;
import java.io.IOException;
import java.util.Stack;

public class NestedReader {
    private StringBuilder nestedString;
    private BufferedReader input;
    char aChar;

    NestedReader(BufferedReader input) {
        this.input = input;
    }

    /*
        This method checks for the valid character inputted from the user and appends it to the string
     */
    String getNestedString() throws IOException {
        nestedString = new StringBuilder();
        Stack<Character> inputChars = new Stack<Character>();
        aChar = (char )input.read();
        while(true){
            if(aChar == (char)-1)
                return null;
            if(aChar == '{' || aChar == '(' || aChar == '[') {
                inputChars.push(aChar);
                nestedString.append(aChar);
            }
            else if (aChar == '/'){
                aChar = (char )input.read();
                if (aChar != '/')
                {
                    nestedString.append('/');
                    continue;
                }
                ignoreAllChars(aChar);
                if(inputChars.size() == 0)
                    return nestedString.toString();
            }
            else if (aChar == '}') {
                if (inputChars.size() == 0 || inputChars.lastElement() != '{') {
                    appendAllChars(aChar);
                    return nestedString.toString();
                }
                nestedString.append(aChar);
                inputChars.pop();
            }
            else if (aChar == ']') {
                if (inputChars.size() == 0 || inputChars.lastElement() != '[') {
                    appendAllChars(aChar);
                    return nestedString.toString();
                }
                nestedString.append(aChar);
                inputChars.pop();
            }
            else if (aChar == ')') {
                if (inputChars.size() == 0 || inputChars.lastElement() != '(') {
                    appendAllChars(aChar);
                    return nestedString.toString();
                }
                nestedString.append(aChar);
                inputChars.pop();
            }
            else if (aChar == '"'){
                if(inputChars.size() == 0) {
                    appendAllChars(aChar);
                    return nestedString.toString();
                }
                nestedString.append(aChar);
            }
            else if (aChar == '\n'){
                if(inputChars.size() == 0){
                    return nestedString.toString();
                }
                nestedString.append(aChar);
            }
            else{
                nestedString.append(aChar);
            }
            aChar = (char )input.read();
        }
    }

    /*
        This method handles the closing brackets and quotations which appends all the characters from now on to the string
     */
    private void appendAllChars(char c) throws IOException{
        while(c != '\n') {
            if(c == '/'){
                c = (char )input.read();
                if(c == '/'){
                    ignoreAllChars(c);
                    break;
                }
                nestedString.append('/');
            }
            nestedString.append(c);
            c = (char )input.read();
        }
    }

    /*
        This method ignores all the characters inputted from now on until a new line has been observed, mainly used for ignoring comments
     */
    private void ignoreAllChars(char c) throws IOException{
        while(c != '\n') {
            c = (char )input.read();
        }
    }
}